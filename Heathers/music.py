from fastapi import Body, FastAPI

app = FastAPI()

MUSIC = [
    {"title": "Number 5 Document", "artist": "R.E.M.", "genre": "Rock"},
    {"title": "In Utero", "artist": "Nirvana", "genre": "Rock"},
    {"title": "Dark Side of the Moon", "artist": "Pink Floyd", "genre": "Rock"},
    {"title": "Van Halen", "artist": "Van Halen", "genre": "Rock"},
    {"title": "Innervisions", "artist": "Stevie Wonder", "genre": "Funk"},
    {"title": "Ultimate", "artist": "Prince", "genre": "Soul"},
    {"title": "Signed, Sealed and Delivered", "artist": "Stevie Wonder", "genre": "Soul"}
]

@app.get("/music/")
async def get_all_music():
    return MUSIC


@app.get("/music/{album_title}")
async def get_music_by_album_title(album_title: str):
    for album in MUSIC:
        if album.get('title').casefold() == album_title.casefold():
            return album


@app.get("/music/artist/")
async def get_music_by_artist(artist_name: str):
    albums_to_return = []

    for album in MUSIC:
        if album.get('artist').casefold() == artist_name.casefold():

            albums_to_return.append(album)
    return albums_to_return


@app.get("/music/genre/")
async def get_music_by_genre(genre: str):
    albums_to_return = []

    for album in MUSIC:
        if album.get('genre').casefold() == genre.casefold():

            albums_to_return.append(album)
    return albums_to_return


@app.get("/music/{artist}/")
async def get_albums_by_artist_and_genre(artist: str, genre: str):
    albums_to_return = []

    for album in MUSIC:
        if album.get('artist').casefold() == artist.casefold() \
        and album.get('genre').casefold() == genre.casefold():

            albums_to_return.append(album)
    return albums_to_return


@app.post("/music/add_album/")
async def add_album(new_album=Body()):
    MUSIC.append(new_album)


@app.put("/music/update_album_genre")
async def update_album_genre(updated_album=Body()):

    for i in range(len(MUSIC)):
        if MUSIC[i].get('title').casefold() == updated_album.get('title').casefold():

            MUSIC[i] = updated_album


@app.delete("/music/delete_album/{album_title}/")
async def delete_album(album_title: str):

    for i in range(len(MUSIC)):
        if MUSIC[i].get('title').casefold() == album_title.casefold():
            MUSIC.pop(i)
            break
